package com.shoebalicious;
import org.opencv.core.Mat;

public class RequestResponse {
    private Mat mat;
    private int operation;
    private float amount;

    public RequestResponse(){
        this.mat = null;
        this.operation = 0;
        this.amount = 0;
    }

    //Request constructor
    public RequestResponse(Mat imageMat, int operation, float amount){
        this.mat = imageMat;
        this.operation = operation;
        this.amount = amount;
    }

    //Response constructor
    public RequestResponse(Mat imageMat){
        this.mat = imageMat;
        this.operation = 9;
        this.amount = 9;
    }

    public Mat getMat() {
        return mat;
    }
    public int getOperation(){
        return operation;
    }
    public float getAmount(){
        return amount;
    }
}
