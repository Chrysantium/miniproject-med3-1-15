package com.shoebalicious;

import org.opencv.core.Mat;

import java.io.*;
import java.net.Socket;

public class ProcessingThread implements Runnable{
    private TCPServer server;
    private Socket clientSocket;
    private DataOutputStream dataOutputStream;
    private DataInputStream dataInputStream;

    public ProcessingThread(TCPServer server, Socket clientSocket){
        this.server = server;
        this.clientSocket = clientSocket;
    }

    public void run(){
        try{
            dataOutputStream = new DataOutputStream(clientSocket.getOutputStream());
            dataInputStream = new DataInputStream(clientSocket.getInputStream());
        } catch (Exception e) {
            System.out.println("Connection error");
            System.exit(-1);
        }

        //Waiting for request
        try {
            byte[] request = SenderReceiver.receivePacket(dataInputStream);
            System.out.println("Request received - Processing");
            RequestResponse source = TransferProtocol.unpackData(request);

            //Request for brightness adjustment
            if (request[0] == 1) {
                try {
                    Mat responseImage = ImageProcessor.brightnessAdjustment(source.getMat(), (int) source.getAmount());
                    byte[] response = TransferProtocol.packData(new RequestResponse(responseImage));
                    SenderReceiver.sendPacket(response, dataOutputStream);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }

            //Request for contrast adjustment
            else if (request[0] == 2) {
                try {
                    Mat responseImage = ImageProcessor.contrastAdjustment(source.getMat(), source.getAmount());
                    byte[] response = TransferProtocol.packData(new RequestResponse(responseImage));
                    SenderReceiver.sendPacket(response, dataOutputStream);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            } else {
                System.out.println("Requested operation not recognised: " + request[0]);
            }
            System.out.println("Response Sent");

            //Close the connection to the client
            dataInputStream.close();
            dataOutputStream.close();
            System.out.println("Disconnected from client");
        } catch (Exception e) {
            System.out.println("Connection to client lost");
            try {
                dataInputStream.close();
                dataOutputStream.close();
            } catch (Exception exception) {
                System.out.println("Error closing the connection: " + exception.getMessage());
            }
        }

        server.finishThread();
    }
}
