package com.shoebalicious;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import java.io.*;
import java.net.Socket;

public class TCPClient {
    private Socket clientSocket;
    private DataOutputStream dataOutputStream;
    private DataInputStream dataInputStream;

    private Mat inputImage;
    private byte[] packedRequest;

    public TCPClient(){
        inputImage = new Mat();
        try {
            clientSocket = new Socket("localhost", TransferProtocol.PORTNUMBER);
        } catch (Exception e) {
            System.out.println("Server not found");
            System.exit(-1);
        }
    }

    public void performRequest(String filepath, String operation, String amount){
        //Load the file from the filepath
        try{
            inputImage = Imgcodecs.imread(filepath);
        } catch (Exception e) {
            System.out.println("Filepath invalid");
            System.exit(-1);
        }

        //Convert operation text to operation numbers
        int operationNumber = 0;
        if (operation.equalsIgnoreCase("brightness")){
            operationNumber = 1;
        } else if (operation.equalsIgnoreCase("contrast")){
            operationNumber = 2;
        }

        //Generate an object containing the request
        RequestResponse request = new RequestResponse(inputImage,operationNumber,Float.parseFloat(amount));

        //Validate the request parameters
        if (request.getMat().size().area() == 0){
            System.out.println("Filepath invalid");
            System.exit(-1);
        }
        if (request.getOperation() <= 0){
            throw new IllegalArgumentException("Operation could not be recognised");
        }
        if (request.getOperation() == 2 && request.getAmount() < 0){
            throw new IllegalArgumentException("Unable to do contrast adjustment by a negative amount");
        }

        //Open streams for communication with the server
        try {
            dataOutputStream = new DataOutputStream(clientSocket.getOutputStream());
            dataInputStream = new DataInputStream(clientSocket.getInputStream());
            System.out.println("Connection to server established");
        } catch (Exception e){
            System.out.println("Client failed to open streams");
        }

        //Create, convert and send packedRequest
        packedRequest = TransferProtocol.packData(request);
        SenderReceiver.sendPacket(packedRequest,dataOutputStream);
        System.out.println("Request sent - Awaiting response");

        //Wait for the server to respond
        byte[] packedResponse = SenderReceiver.receivePacket(dataInputStream);
        RequestResponse response = TransferProtocol.unpackData(packedResponse);
        System.out.println("Response received - Printing result to output/result.jpg");
        Imgcodecs.imwrite("output/result.jpg", response.getMat());

        //Close the connection to the server
        try {
            dataInputStream.close();
            dataOutputStream.close();
            System.out.println("Disconnected from server");
        } catch (Exception exception) {
            System.out.println("Error closing the connection: " + exception.getMessage());
        }
    }
}
