package com.shoebalicious;
import org.opencv.core.Mat;
import org.opencv.core.CvType;

public class TransferProtocol {
    public static final int PORTNUMBER = 2244;
    public static final int OVERHEAD_SIZE = 8;

    public static byte[] packData(RequestResponse data){
        byte[] packet = new byte[OVERHEAD_SIZE + (int)data.getMat().size().area()*data.getMat().channels()];
        Mat source = data.getMat();

        packet[0] = (byte)data.getOperation();

        float amount = Math.max(-255, Math.min(255, data.getAmount()));
        if (amount < 0) {
            packet[1] = (byte) 1;
        }

        if (data.getOperation() == 2){
            amount *= 10;
        }

        packet[2] = (byte)(amount - 128);
        packet[3] = (byte)Math.floor(source.height() / 256);
        packet[4] = (byte)(source.height()-128);
        packet[5] = (byte)Math.floor(source.width() / 256);
        packet[6] = (byte)(source.width()-128);
        packet[7] = (byte)source.channels();

        for (int i = 0 ; i < source.height() ; i++) {
            for (int j = 0; j < source.width(); j++) {
                for (int k = 0; k < source.channels(); k++) {
                    packet[(i*source.width()*source.channels()+j*source.channels()+k+OVERHEAD_SIZE)] = (byte)(source.get(i, j)[k]-128);
                }
            }
        }

        return packet;
    }

    public static RequestResponse unpackData(byte[] packet){
        int operation = packet[0];
        float amount;

        if (packet[1] == 1) {
            amount = packet[2] - 128;
        } else {
            amount = packet[2] + 128;
        }

        if (packet[0] == 2){
            amount /= 10;
        }

        int height = packet[3]*256+packet[4]+128;
        int width = packet[5]*256+packet[6]+128;
        Mat output = new Mat(height,width,CvType.CV_8UC(packet[7]));

        for (int i = 0 ; i < height ; i++) {
            for (int j = 0; j < width; j++) {
                byte[] value = new byte[3];
                for (int k = 0; k < packet[7]; k++) {
                    value[k] = (byte)(packet[(i*width*packet[7]+j*packet[7]+k+OVERHEAD_SIZE)]+128);
                }
                output.put(i,j,value);
            }
        }

        return new RequestResponse(output,operation,amount);
    }
}

