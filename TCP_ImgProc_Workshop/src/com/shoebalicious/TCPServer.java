package com.shoebalicious;

import java.net.*;

public class TCPServer {
    private ServerSocket serverSocket;
    private int clientCount;

    public TCPServer(){
        try {
            serverSocket = new ServerSocket(TransferProtocol.PORTNUMBER);
        } catch (IllegalArgumentException e){
            System.out.println("Invalid Port");
            System.exit(-1);
        } catch (Exception e){
            System.out.println("Server failed to initialize");
            System.exit(-1);
        }
        clientCount = 0;
    }

    public void startServer() {
        //Continually waits for clients to connect
        while (true) {
            try {
                //Setup connection to the client
                System.out.println("Waiting for client to connect");
                Socket clientSocket = serverSocket.accept();
                new Thread(new ProcessingThread(this, clientSocket)).start();
                clientCount++;
                System.out.println("New client connected. Requests being processed: " + clientCount);
            } catch (Exception e) {
                System.out.println("Connection error");
                System.exit(-1);
            }
        }
    }

    public void finishThread(){
        clientCount--;
        System.out.println("Requests being processed: " + clientCount);
    }
}