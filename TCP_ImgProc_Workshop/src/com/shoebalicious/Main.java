package com.shoebalicious;

import org.opencv.core.*;

public class Main {
    public static void main(String[] args) {
        // java -jar TCP_ImgProc_Workshop.jar <client|server> <filepath> <operation> <amount>
        // Operation: brightness {amount: -255 to 255}, contrast {amount: 0 to inf.}

        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

        if(args.length > 0)
        {
            if(args[0].equalsIgnoreCase("client")){
                if(args.length != 4){
                    //Add a check if the specified request is a valid file, i.e. a picture
                    System.out.println("Illegal arguements, please specify: <client|server> <filepath> <operation> <amount>");
                    System.exit(-1);
                }
                TCPClient client = new TCPClient();

                try {
                    client.performRequest(args[1], args[2], args[3]);
                } catch (IllegalArgumentException e){
                    System.out.print("Illegal argument: ");
                    System.out.println(e.getMessage());
                    System.exit(-1);
                }
                System.exit(1);
            } else if(args[0].equalsIgnoreCase("server")){
                TCPServer server = new TCPServer();
                server.startServer();
            } else {
                throw new IllegalArgumentException("argument not valid");
            }
        }
    }
}
