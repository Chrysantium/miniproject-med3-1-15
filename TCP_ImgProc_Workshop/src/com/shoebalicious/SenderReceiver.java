package com.shoebalicious;

import java.io.*;

public class SenderReceiver {
    public static void sendPacket(byte[] packet, DataOutputStream dataOutputStream){
        //Send the length of the packet followed by the content
        try {
            dataOutputStream.writeInt(packet.length);
            dataOutputStream.write(packet);
        } catch (Exception e) {
            System.out.println("Failed to send packet");
            System.exit(-1);
        }
    }

    public static byte[] receivePacket(DataInputStream dataInputStream){
        //Wait for the length of a new packet to arrive followed by the content
        try {
            int dataLength = dataInputStream.readInt();
            byte[] packet = new byte[dataLength];
            dataInputStream.readFully(packet);
            return packet;
        } catch (IOException e){
            System.out.print(e.getMessage());
            return null;
        }
    }
}
