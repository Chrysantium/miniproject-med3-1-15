package com.shoebalicious;
import org.opencv.core.Mat;

public class ImageProcessor {

    public static Mat brightnessAdjustment(Mat input, int amount){
        Mat output = new Mat(input.size(),input.type());

        for(int i = 0; i < input.height();i++){
            for(int j = 0; j < input.width();j++){
                double[] value = new double[3];
                value[0] = (int)input.get(i,j)[0]+amount;
                value[1] = (int)input.get(i,j)[1]+amount;
                value[2] = (int)input.get(i,j)[2]+amount;
                output.put(i,j,value);
            }
        }
        return output;
    }

    public static Mat contrastAdjustment(Mat input, float amount){
        Mat output = new Mat(input.size(),input.type());

        for(int i = 0; i < input.height();i++){
            for(int j = 0; j < input.width();j++){
                double[] value = new double[3];
                value[0] = (int)(input.get(i,j)[0]*amount);
                value[1] = (int)(input.get(i,j)[1]*amount);
                value[2] = (int)(input.get(i,j)[2]*amount);
                output.put(i,j,value);
            }
        }
        return output;
    }
}
